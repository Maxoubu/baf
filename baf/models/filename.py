# -*- coding: utf-8 -*-
import re
import logging

import sqlalchemy as sa
from sqlalchemy import orm

from .common import Common
from .meta import Base

log = logging.getLogger(__name__)


class FileName(Base, Common):
    __tablename__ = 'filename'
    __table_args__ = (
        sa.UniqueConstraint('path', 'name', 'folder_id'),
    )
    id = sa.Column(sa.Integer, primary_key=True)
    path = sa.Column(sa.Text, index=True, nullable=False)
    name = sa.Column(sa.Text, index=True, nullable=False)
    typeinfo = sa.Column(sa.Text)
    inode = sa.Column(sa.Integer)
    mtime = sa.Column(sa.Integer)
    ctime = sa.Column(sa.Integer)
    size = sa.Column(sa.Integer)
    uid = sa.Column(sa.Integer)
    gid = sa.Column(sa.Integer)
    filecontent_id = sa.Column(sa.Integer, sa.ForeignKey("filecontent.id"))
    filetype_id = sa.Column(sa.Integer, sa.ForeignKey("filetype.id"))
    folder_id = sa.Column(sa.Integer, sa.ForeignKey("folder.id"))

    filecontent = orm.relationship('FileContent')
    filetype = orm.relationship('FileType')
    folder = orm.relationship('Folder')

    filename_re = re.compile(r'^(?P<filename>.+)\.(?P<extension>[^\.]+)$')

    @classmethod
    def from_path_name_and_folder_id(cls, dbsession, path, name, folder_id):
        return dbsession.query(cls).filter(
            cls.path == path, cls.name == name, cls.folder_id == folder_id
        ).first()

    def extension(self):
        if self.name:
            m = self.filename_re.match(self.name)
            if m:
                return m.group('extension')
        return None
